# GitLab's Helm Stable archive

This project archives certain Helm charts used by GitLab, namely:

- [nginx-ingress](https://github.com/helm/charts/tree/master/stable/nginx-ingress)
- [prometheus](https://github.com/helm/charts/tree/master/stable/prometheus)
- [fluentd](https://github.com/helm/charts/tree/master/stable/fluentd)
- [postgresql](https://github.com/helm/charts/tree/master/stable/postgresql)

These charts were previous hosted at the official Helm stable
repository at https://kubernetes-charts.storage.googleapis.com/, but
that repository will be gone on 2020-11-13.


## Steps to generate

1. Run `scripts/download_index_yaml`. This downloads
   https://kubernetes-charts.storage.googleapis.com/index.yaml to
   `scripts/data/index.yaml`.
1. Run `scripts/download_files` in the directory where we want to save
   the charts. For this project we will create a `public/charts/`
   directory.
1. In the `public` directory, run `helm repo index --url https://gitlab-org.gitlab.io/cluster-integration/helm-stable-archive .` and commit the result
